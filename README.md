# visr

analysis and experimental code for visual recollection


## Install

Package dependency is ideally handled by a simple call to `packrat::restore()`

```R
# If devtools isn't installed
install.packages("devtools")
# require development version for install_gitlab
devtools::instal_github("r-lib/devtools")
devtools::install_gitlab("psadil/visr")
```


