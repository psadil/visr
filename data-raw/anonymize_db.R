#!/usr/bin/env Rscript
#
# Anonymize participants database
# NOTE: always overrides the file --outfile
#

suppressMessages(library(docopt)) 

"Usage: 
  anonymize_db.R [-i DBNAME] KEY OUTFILE
  anonymize_db.R -h

Options:
  -i --infile DBNAME     sqlite database filename from which to read [default: participants_raw.db]
  -t --table TABLE       name of table within database to anonymize [default: participants]
  -h --help              show this help text

Arguments:
  KEY                   key to salt WorkerIDs for extra security
  OUTFILE               sqlite database filename to write" -> doc


opt <- docopt(doc)

suppressMessages(library(magrittr))
suppressMessages(library(dplyr))
suppressMessages(library(stringr))
suppressMessages(library(openssl))


db <- dplyr::src_sqlite(opt$infile) %>%
  dplyr::tbl(opt$table) %>%
  dplyr::collect() %>%
  dplyr::mutate(uniqueid = stringr::str_replace(uniqueid, 
                                                workerid, 
                                                openssl::sha256(workerid, key = opt$KEY)),
                datastring = dplyr::case_when(is.na(datastring) ~ datastring,
                                              TRUE ~ stringr::str_replace_all(datastring, 
                                                                              workerid, 
                                                                              openssl::sha256(workerid, key = opt$KEY))),
                workerid = openssl::sha256(workerid, key = opt$KEY)
  )
message(paste0("read raw database: ", opt$infile))

con <- DBI::dbConnect(RSQLite::SQLite(), opt$OUTFILE)
dplyr::copy_to(con, db, opt$table,   
               temporary = FALSE, 
               indexes = list(
                 "uniqueid" 
               ),
               overwrite = TRUE)

DBI::dbDisconnect(con)
message(paste0("wrote anonymized database: ", opt$OUTFILE))
message(paste0("Store your KEY securely if you want the same WorkerIDs to create the same HMACs!"))