library(tidyverse)
library(magrittr)
devtools::load_all()


con <- DBI::dbConnect(RPostgres::Postgres(),
                      dbname = 'd4kueqmunhql3n',
                      host = "ec2-54-227-241-179.compute-1.amazonaws.com",
                      user = "pdipocnvcdkezp",
                      port = 5432,
                      password = rstudioapi::askForPassword("Database password")
)

d <- con %>%
  tbl("participants") %>%
  filter(mode == "live" & codeversion == "61") %>%
  collect() %>%
  select(-beginexp, -endhit)

d <- .anonymize(d) %>%
  mutate(codeversion = as.numeric(codeversion)) %>%
  filter(status >= 3) %>%
  mutate(json = map(datastring, ~ .x %>%
                      jsonlite::fromJSON() %>%
                      extract2('data') %>%
                      extract2('trialdata') %>%
                      as_tibble() )) %>%
  unnest(json) %>%
  select(-bonus, -mode, -ipaddress, -browser, -platform, -language, -datastring, -status) %>%
  mutate(trial_type = case_when(trial_type == "html-button-response" ~ "study",
                                trial_type == "2afc-button-response" ~ "2afc",
                                trial_type == "image-textbox-response" ~ "name")) %>%
  mutate(choices = map(choices, ~str_replace(.x, "&#8217;", "'"))) %>% # child's rattle, with apostrophe
  rename(list = counterbalance)


afc <- d %>%
  filter(trial_type == "2afc") %>%
  mutate(object = map_int(stimulus, ~str_extract(.x, '[[:digit:]]{3}')[1] %>% as.integer()),
         correct_side = map_chr(stimulus, ~.check_correct_side(.x)),
         afc_result = if_else(button_pressed == correct_side, TRUE, FALSE)) %>%
  select(-stimulus, -trial_index, -trial_type, -input)

targets <- read_csv(file.path("data-raw", "visr-mturk","static","images","objects", "names.csv"), col_types = cols(
  name1 = col_character(),
  name2 = col_character(),
  name3 = col_character(),
  name4 = col_character(),
  name5 = col_character(),
  name6 = col_character(),
  name7 = col_character(),
  name8 = col_character(),
  name9 = col_character(),
  name10 = col_character(),
  name11 = col_character()
))

naming <- d %>%
  filter(trial_type == "name") %>%
  mutate(object = map_int(stimulus, ~str_extract(.x, '[[:digit:]]{3}')[1] %>% as.integer())) %>%
  rename(chosen_name = input) %>%
  select(-stimulus, -choices, -trial_type, -button_pressed) %>%
  mutate(chosen_name = stringr::str_to_lower(chosen_name)) %>%
  .check_names(., chosen_name, targets)


study <- read_csv(file.path("data-raw", "study.csv"), col_types = cols(
  cond = col_integer(),
  list = col_integer(),
  object = col_integer(),
  study_code = col_integer(),
  pair = col_integer(),
  study_type = col_character(),
  n_study_reps = col_integer(),
  rep = col_integer(),
  name1 = col_character(),
  study_question = col_integer()
) ) %>%
  filter(rep == 1) %>%
  select(-rep, -study_question)

test <- read_csv(file.path("data-raw","test.csv"), col_types = cols(
  list = col_integer(),
  cond = col_integer(),
  pair = col_integer(),
  object = col_integer(),
  name1 = col_character()
) )

d <- inner_join(afc, naming, by = c("uniqueid",
                                    "assignmentid",
                                    "workerid",
                                    "hitid",
                                    "object",
                                    "cond",
                                    "list",
                                    "codeversion"), suffix = c(".afc",".name")) %>%
  inner_join(test, by = c("object","list","cond")) %>%
  left_join(study, by = c("list", "object","pair","name1", "cond","list"), suffix = c(".test",".study")) %>%
  mutate(study_type = if_else(is.na(study_type), "not studied", study_type),
         n_study_reps = if_else(is.na(n_study_reps), 0L, n_study_reps),
         study_code = if_else(is.na(study_code), 1L, study_code)) %>%
  filter(!workerid == "edef37e02ba1f47cd5fa8b3204881a125b640bed263ac5103f62ae562cbb37be")


usethis::use_data(d, overwrite = TRUE)
DBI::dbDisconnect(con)

d %>%
  filter(!name_result &
           !chosen_name=="" &
           !str_detect(chosen_name, "know") &
           !str_detect(chosen_name, "no idea") &
           !str_detect(chosen_name, "can't tell") &
           !str_detect(chosen_name, "don't remember") &
           !str_detect(chosen_name, "not sure") &
           !str_detect(chosen_name, "unsure")) %>%
  select(chosen_name, name1, object, workerid) %>%
  write_csv(file.path("data-raw", "names_marked_incorrect.csv"))

# last participant 5e8fe357dee7bb35f343d1d734c2b16b8636fb89d5017f821b5a71aff05ffbec

